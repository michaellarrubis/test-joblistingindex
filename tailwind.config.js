module.exports = {
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'c-teal': '#319795',
        'c-blue': '#3182CE',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
